const server = require('./server/index');
const {PORT, MONGO_URI} = require('./config');
const mongoose = require('mongoose');

mongoose.connect(MONGO_URI, {useUnifiedTopology:true, useNewUrlParser:true}).then(()=>{
    server.listen(PORT, ()=>{
        console.log(`Server on port ${PORT}`);
    });
})
.catch(console.log);


